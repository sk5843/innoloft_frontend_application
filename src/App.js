import React from 'react';
import './App.scss';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import UserInputTabs from './components/UserInputTabs';

function App() {
  return (
    <>
      <Header />
      <main className="main">
        <Sidebar />
        <UserInputTabs />
      </main>
    </>
  );
}

export default App;
