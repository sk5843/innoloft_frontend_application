import React, { useState, useEffect } from 'react';
import Form1 from './Form1';
import Form2 from './Form2';
import axios from 'axios';

const UserInputTabs = () => {
  const [selectedTab, setSelectedTab] = useState('1');
  const [error, setError] = useState('');
  const [userinfo, setUserInfo] = useState({
    email: 'test@gmail.com',
    password: '',
    retypedPassword: '',
    firstname: 'John',
    lastname: 'Doe',
    housenumber: '45/A',
    street: 'Mohrenstraße 30, Berlin',
    postal: '10117',
    country: 'Germany',
  });
  const {
    firstname,
    lastname,
    housenumber,
    street,
    postal,
    country,
    password,
    retypedPassword,
  } = userinfo;
  useEffect(() => {
    //Form validation
    if (retypedPassword !== password) {
      setError('Passwords do not match');
    } else if (
      firstname === '' ||
      lastname === '' ||
      housenumber === '' ||
      street === '' ||
      postal === '' ||
      country === ''
    ) {
      setError('* One or more fields are required');
    } else {
      setError('');
    }
  }, [
    password,
    retypedPassword,
    firstname,
    lastname,
    housenumber,
    street,
    postal,
    country,
  ]);

  const handleChange = (e) => {
    const value = e.target.value;
    setUserInfo({
      ...userinfo,
      [e.target.name]: value,
    });
  };
  const handlePasswordChange = (state) => {
    setUserInfo({
      ...userinfo,
      password: state.password,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    //Fake AJAX call to update user data. This can be done using axios to perform a post request to the REST API or
    //if we're using GRAPHQL, using gql mutation queries or if using firebase, using firestore functions.
    try {
      const status = await axios.post('/fakepath', userinfo);
      alert('Data has been saved successfully');
    } catch (error) {
      alert(error);
    }
  };

  return (
    <div className="userinfo">
      <div className="userinfo__tabs">
        <div
          className={`userinfo__tab ${
            selectedTab === '1' && 'userinfo__tab--selected'
          }`}
          onClick={() => setSelectedTab('1')}
        >
          Main information
        </div>
        <div
          className={`userinfo__tab ${
            selectedTab === '2' && 'userinfo__tab--selected'
          }`}
          onClick={() => setSelectedTab('2')}
        >
          Additional information
        </div>
      </div>
      <form className="userinfo__form" onSubmit={(e) => handleSubmit(e)}>
        {selectedTab === '1' ? (
          <Form1
            userinfo={userinfo}
            handleChange={handleChange}
            handlePasswordChange={handlePasswordChange}
            error={error}
          />
        ) : (
          <Form2
            userinfo={userinfo}
            handleChange={handleChange}
            error={error}
          />
        )}
        <p className="userinfo__error">{error}</p>
        <button
          className={`userinfo__submit ${
            error !== '' && 'userinfo__submit--disabled'
          }`}
          type="submit"
          disabled={error !== ''}
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default UserInputTabs;
