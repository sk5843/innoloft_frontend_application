import React from 'react';

const SidebarItem = ({ icon, title }) => {
  return (
    <li className="navbar__item">
      <span>{icon}</span>
      {title}
    </li>
  );
};
export default SidebarItem;
