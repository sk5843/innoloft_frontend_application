import React from 'react';
import SidebarItem from './SidebarItem';
import {
  FaHome,
  FaUserCircle,
  FaRegBuilding,
  FaTools,
  FaRegNewspaper,
  FaSortAlphaUpAlt,
} from 'react-icons/fa';

const Sidebar = () => {
  return (
    <section className="sidebar">
      <nav className="navbar">
        <ul className="navbar__list">
          <SidebarItem icon={<FaHome />} title="Home" />
          <SidebarItem icon={<FaUserCircle />} title="My Account" />
          <SidebarItem icon={<FaRegBuilding />} title="My Company" />
          <SidebarItem icon={<FaTools />} title="My Settings" />
          <SidebarItem icon={<FaRegNewspaper />} title="News" />
          <SidebarItem icon={<FaSortAlphaUpAlt />} title="Analytics" />
        </ul>
      </nav>
    </section>
  );
};
export default Sidebar;
