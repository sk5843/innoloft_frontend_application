import React from 'react';
import ReactPasswordStrength from 'react-password-strength';

const Form1 = ({ userinfo, handleChange, handlePasswordChange, error }) => {
  const { email, password, retypedPassword } = userinfo;

  return (
    <div className="userinfo__tab1">
      <label htmlFor="email">New Email address: </label>
      <input
        type="email"
        className="userinfo__email"
        name="email"
        placeholder="Email"
        value={email}
        onChange={(e) => handleChange(e)}
      />
      <label htmlFor="password">New Password: </label>
      <ReactPasswordStrength
        minLength={6}
        inputProps={{
          type: 'password',
          className: 'userinfo__password',
          name: 'password',
          placeholder: 'Password',
          value: { password },
        }}
        changeCallback={(state) => handlePasswordChange(state)}
      />

      <label htmlFor="password">Retype Password: </label>
      <input
        type="password"
        className="userinfo__retypepassword"
        name="retypedPassword"
        placeholder="Password"
        value={retypedPassword}
        onChange={(e) => handleChange(e)}
      />
    </div>
  );
};

export default Form1;
