import React from 'react';

const Form2 = ({ userinfo, handleChange, error }) => {
  const {
    firstname,
    lastname,
    housenumber,
    street,
    country,
    postal,
  } = userinfo;
  return (
    <div className="userinfo__tab2">
      <div className="userinfo__name">
        <input
          type="text"
          className="userinfo__firstname"
          name="firstname"
          placeholder="First Name"
          value={firstname}
          required
          onChange={(e) => handleChange(e)}
        />
        <input
          type="text"
          className="userinfo__lastname"
          name="lastname"
          placeholder="Last Name"
          value={lastname}
          required
          onChange={(e) => handleChange(e)}
        />
      </div>
      <p>Address: </p>
      <div className="userinfo__address">
        <input
          type="text"
          className="userinfo__housenumber"
          name="housenumber"
          placeholder="Flat No"
          value={housenumber}
          required
          onChange={(e) => handleChange(e)}
        />
        <input
          type="text"
          className="userinfo__street"
          name="street"
          placeholder="Street"
          value={street}
          required
          onChange={(e) => handleChange(e)}
        />
        <input
          type="text"
          className="userinfo__postal"
          name="postal"
          placeholder="PIN code"
          value={postal}
          required
          onChange={(e) => handleChange(e)}
        />
        <select
          name="country"
          className="userinfo__country"
          value={country}
          required
          onChange={(e) => handleChange(e)}
        >
          <option>Germany</option>
          <option>Switzerland</option>
          <option>Austria</option>
        </select>
      </div>
    </div>
  );
};

export default Form2;
