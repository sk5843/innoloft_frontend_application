import React from 'react';
import logo from '../images/energieloft_logo.png';
import { FaGlobeEurope, FaEnvelope, FaBell } from 'react-icons/fa';

const Header = () => {
  return (
    <header className="header">
      <img className="header__logo" src={logo} alt="energieloft" />
      <nav className="navbar">
        <ul className="navbar__list">
          <li className="navbar__item">
            <span>
              <FaGlobeEurope />
            </span>
            EN
          </li>
          <li className="navbar__item navbar__item--dot">
            <FaEnvelope />
          </li>
          <li className="navbar__item navbar__item--dot">
            <FaBell />
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
